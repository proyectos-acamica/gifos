let validateLocalStorage = (type) => {
   let getLocalStorage;

   getLocalStorage = localStorage.getItem(type);

   return getLocalStorage === null ? false : true;
};


let isMenuActive = (event) =>{
   const element = event.target
   const isMenuActive = element.dataset.statusMenu;
   const sendNewStatusMenu = (isMenuActive === 'inactivo') ? 'activo': 'inactivo'
   
   element.dataset.statusMenu = sendNewStatusMenu

   return sendNewStatusMenu == 'activo' ? true: false 
}


let validationScreen = () =>{
   const screen = window.screen;
   const objResults ={
      width: screen.width,
      height: screen.height,
      orientation: screen.orientation.type
   }

   return objResults
}

let validationTheme = () => {
   let getLocalStorage =  localStorage.getItem("theme");
   let body = document.querySelector('body');
   body.dataset.typeTheme = (getLocalStorage === null ) ? 'dark' : getLocalStorage; 
   
}

export {validateLocalStorage, isMenuActive, validationScreen, validationTheme};

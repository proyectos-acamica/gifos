import {config} from './data.js'
import {createButtonCards} from './cards.js'

const {API_KEY, URL_GIPHY_UPLOAD, URL_GIPHY_GET_GIF} = config;
const messageTime = document.querySelector('.create__plus');
const arrSrcIconsGif =  
   {
      loader:{
         src: './img/icon-loader.svg'
      },
      check:{
         src: './img/icon-check.svg'
      },
      download:{
         src: './img/icon-download-light.svg'
      },
      link:{
         src: './img/icon-link.svg'
      }
   }
let recorder;
let mediaStream;
let gifCreated;
let initiation = 0;
let timeout = 0;

let requestCameraPermissions = async () => {

   const containerTime = document.querySelector('.create__infoAdditional');
   containerTime.classList.add('create__infoAdditional--inactive');

   setMessageStep("1");
   validateStepActive("1");
   try {
      mediaStream = await navigator.mediaDevices.getUserMedia({
         audio: false,
         video: {
            
         },
      });
      setDataVideo(mediaStream);
   } catch (error) {
      const CODE_ERROR_NOT_DEVICE = 8
      if(error.code === CODE_ERROR_NOT_DEVICE){
         setTimeout(() => {
            setMessageStep('8');
         }, 5000);
      }else{
         setTimeout(() => {
            setMessageStep('9');
         }, 5000);
      }
   }
};

let setDataVideo = (mediaStream) => {
   const containerGifo = document.querySelector("#containerGifo");
   const containerVideo = document.createElement("DIV");
   const containerButtons = document.createElement("DIV");
   const video = document.createElement("VIDEO");
   const containerImg = document.createElement("SPAN");

   const containerCreateGif = document.createElement("DIV");
   const icon = document.createElement("IMG");
   const textAlternate = document.createElement("P");

   containerCreateGif.classList.add('create__message-gif');
   containerCreateGif.classList.add('create__message-gif--inactive');
   icon.classList.add('create__icon');
   icon.classList.add('create__icon--loader');
   textAlternate.classList.add('create__text-alternate');

   icon.src = arrSrcIconsGif.loader.src;
   
   textAlternate.innerHTML = 'Estamos subiendo tu GIFO';

   containerCreateGif.appendChild(icon);
   containerCreateGif.appendChild(textAlternate);

   setMessageStep("2");
   validateStepActive("2");

   video.srcObject = mediaStream;
   video.classList.add('create__video');
   video.classList.add('create__video--inactive');

   containerVideo.classList.add('create__container-video');

   containerButtons.classList.add('create__buttons');
   containerButtons.dataset.className = 'create';

   containerImg.classList.add('create__img');
   containerImg.setAttribute("src", '');

   containerVideo.appendChild(video);
   containerVideo.appendChild(containerButtons);
   containerVideo.appendChild(containerImg);

   containerGifo.appendChild(containerVideo);
   containerGifo.appendChild(containerCreateGif);

   video.play();
};

let setMessageStep = (step) => {
   const containerGifo = document.querySelector("#containerGifo");
   const containerButton = document.querySelector("#containerButton");
   const firstTitle = document.createElement("H2");
   const secondTitle = document.createElement("H2");
   const firstSubtitle = document.createElement("P");
   const secondSubtitle = document.createElement("P");
   const button = document.createElement("SPAN");
   const alternateText = document.createElement("SPAN");
   const dividingLine = document.createElement("SPAN");

   firstTitle.classList.add("create__title");

   secondTitle.classList.add("create__title");

   firstSubtitle.classList.add("create__subtitle");

   secondSubtitle.classList.add("create__subtitle");

   dividingLine.classList.add("create__line");
   
   button.classList.add("button");
   button.setAttribute('id', 'btnCreate');

   containerGifo.innerHTML = '';
   containerButton.innerHTML = '';
   
   switch (step) {
      case "1":

         firstTitle.innerHTML = "¿Nos das acceso";

         secondTitle.innerHTML = "a tu cámara?";
         secondTitle.appendChild(alternateText);

         firstSubtitle.innerHTML = "El acceso a tu cámara será válido sólo";
         secondSubtitle.innerHTML ="por el tiempo en el que estés creando el GIFO."

         containerButton.appendChild(dividingLine);

         containerGifo.appendChild(firstTitle);
         containerGifo.appendChild(secondTitle);
         containerGifo.appendChild(firstSubtitle);
         containerGifo.appendChild(secondSubtitle);

         break;
      case "2":
         const containerMessage = document.createElement('DIV');

         containerMessage.classList.add("create__message")

         firstTitle.innerHTML = "Es hora";

         alternateText.classList.add("home__span");
         alternateText.innerHTML = "GIFO";

         secondTitle.innerHTML = "de crear tu primer ";
         secondTitle.appendChild(alternateText);

         firstSubtitle.innerHTML = "Para crear tu GIFO";
         secondSubtitle.innerHTML ="¡Oprime sobre el botón grabar! "

         button.innerHTML = "GRABAR"
         button.setAttribute('data-button-action', 'Grabar');

         containerButton.appendChild(dividingLine);
         containerButton.appendChild(button);

         containerMessage.appendChild(firstTitle);
         containerMessage.appendChild(secondTitle);
         containerMessage.appendChild(firstSubtitle);
         containerMessage.appendChild(secondSubtitle);

         containerGifo.appendChild(containerMessage);

         button.addEventListener('click', startRecording );

         break
      case "8":

         firstTitle.innerHTML = "Ooops...";
         secondTitle.innerHTML = "¡No encontramos tu cámara!";
         secondTitle.appendChild(alternateText);

         firstSubtitle.innerHTML = "Al parecer no cuentas con una";
         secondSubtitle.innerHTML = "cámara que podamos usar para crear el GIFO.";

         containerButton.appendChild(dividingLine);

         containerGifo.appendChild(firstTitle);
         containerGifo.appendChild(secondTitle);
         containerGifo.appendChild(firstSubtitle);
         containerGifo.appendChild(secondSubtitle);
         break
      case "9":
         firstTitle.innerHTML = "Ooops...";
         secondTitle.innerHTML = "¡Necesitamos tu autorización!";
         secondTitle.appendChild(alternateText);

         firstSubtitle.innerHTML = "El acceso a tu cámara será válido sólo";
         secondSubtitle.innerHTML = "por el tiempo en el que estés creando el GIFO.";

         containerButton.appendChild(dividingLine);

         containerGifo.appendChild(firstTitle);
         containerGifo.appendChild(secondTitle);
         containerGifo.appendChild(firstSubtitle);
         containerGifo.appendChild(secondSubtitle);
         break
      default:

         firstTitle.innerHTML = "Aquí podrás";

         alternateText.classList.add("home__span");
         alternateText.innerHTML = "GIFOS";

         secondTitle.innerHTML = "crear tus propios ";
         secondTitle.appendChild(alternateText);

         firstSubtitle.innerHTML = "¡Crea tu GIFO en solo 3 pasos!";
         secondSubtitle.innerHTML ="(Solo necesitas una cámara para grabar un video)"

         button.innerHTML = "COMENZAR"

         containerButton.appendChild(dividingLine);
         containerButton.appendChild(button);

         containerGifo.appendChild(firstTitle);
         containerGifo.appendChild(secondTitle);
         containerGifo.appendChild(firstSubtitle);
         containerGifo.appendChild(secondSubtitle);
         
         break;
   }
};

let validateStepActive = (step) => {
   const containerUl = document.querySelectorAll(".create__ul .create__li");

   containerUl.forEach((li) => {
      if (li.dataset.ordenStep == step) {
         li.classList.add("create__li--active");
      } else {
         li.classList.remove("create__li--active");
      }
   });
};

let startRecording = (event) =>{   
   const buttonAction = event.target.dataset.buttonAction
   const button = event.target;
   if(buttonAction === 'Grabar'){
      
      const video = document.querySelector('.create__video');
      const containerMessage = document.querySelector('.create__message');
      const containerTime = document.querySelector('.create__infoAdditional');
      video.classList.remove('create__video--inactive');
      containerMessage.classList.add('create__message--inactive');
      
      recorder = RecordRTC (mediaStream, {
         type: 'gif',
         disableLogs: false,
         frameRate: 1,
         quality: 10,
         width: 360,
         height: 240,
         onGifRecordingStarted(){
            console.log("started");
         }
      });

      messageTime.classList.remove('create__plus--active');
      
      containerTime.classList.remove('create__infoAdditional--inactive');

      initiation = new Date().getTime();
      chronometer();

      recorder.startRecording();
      button.innerHTML = 'FINALIZAR';
      button.dataset.buttonAction = 'Finalizar';



   }else if (buttonAction === 'Finalizar'){
      recorder.stopRecording(getDataGif);
      clearInterval(timeout);
      timeout = 0;

      const video = document.querySelector('.create__video');

      video.pause();
      
      messageTime.innerHTML = 'REPETIR CAPTURA';
      messageTime.classList.add('create__plus--active');
      messageTime.addEventListener('click', () =>{
         requestCameraPermissions();
      });

      button.innerHTML = 'SUBIR GIFO';
      button.dataset.buttonAction = 'Subir Gifo';


   }else{
      uploadGifo(gifCreated);
      
      const video = document.querySelector('.create__video');
      video.classList.add('create__video--upload');

      const containerVideo = document.querySelector('.create__container-video');
      containerVideo.classList.add('create__container-video--upload');
      
      const containerMessage = document.querySelector('.create__message-gif');
      containerMessage.classList.remove('create__message-gif--inactive');
      
      const containerTime = document.querySelector('.create__infoAdditional');
      containerTime.classList.add('create__infoAdditional--inactive');
      
      validateStepActive("3");

      button.classList.add('button--inactive');
   }
   
}

let getDataGif = () =>{
   gifCreated = new FormData();
   gifCreated.append('file', recorder.getBlob(), 'myGif.gif');
}

let chronometer = () =>{

   const current = new Date().getTime();
   const difference = new Date(current - initiation);
   const minutes = (difference.getUTCMinutes() < 10) ? `0${difference.getUTCMinutes()}` : difference.getUTCMinutes();
   const seconds = (difference.getUTCSeconds() < 10) ? `0${difference.getUTCSeconds()}` : difference.getUTCSeconds();

   messageTime.innerHTML = `00:${minutes}:${seconds}`;

   timeout = setTimeout( 
      chronometer
   ,1000);

}

let uploadGifo = async (gifCreated) =>{
   try {
      const endpoint = `${URL_GIPHY_UPLOAD}?api_key=${API_KEY}`;
      const setData = await fetch(endpoint,{
         method: 'POST',
         body: gifCreated
      });
      const response =  await setData.json();
      
      const gifoId = response.data.id;
      getGifoById(gifoId);
   } catch (error) {
      console.error('Hubo un problema al subir el gif. ', error);
   }

}

let getGifoById = async (gifoId) =>{
   const endpoint = `${URL_GIPHY_GET_GIF}/${gifoId}?api_key=${API_KEY}`;
   const getData = await fetch(endpoint);
   const getDataJSON = await getData.json();
   setGifoById(getDataJSON);
}

let setGifoById = (getDataJSON) => {

   const icon = document.querySelector('.create__icon');
   icon.classList.remove('create__icon--loader');
   icon.src = arrSrcIconsGif.check.src;

   const message = document.querySelector('.create__text-alternate');
   message.innerHTML = 'GIFO subido con exito';

   const isGifosLocalStorage = localStorage.getItem('myGifos') === null ? false : true;
   let date = new Date();
   let title = `MyGifo - ${date.getDate()}-${date.getMonth()}-${date.getFullYear()}`

   if(isGifosLocalStorage){
      const getArrGifos = JSON.parse(localStorage.getItem('myGifos'));

      let objGifos = 
         {
            username: getDataJSON.data.user.username,
            id: getDataJSON.data.id,
            images: {
               fixed_height: {
                  url: getDataJSON.data.images.fixed_height.url,
               },
            },
            title,
         }
      ;

      getArrGifos.data.push(objGifos);
      localStorage.setItem("myGifos", JSON.stringify(getArrGifos));

   }else{
      let objGifos = [
         {
            username: getDataJSON.data.user.username,
            id: getDataJSON.data.id,
            images: {
               fixed_height: {
                  url: getDataJSON.data.images.fixed_height.url,
               },
            },
            title,
         },
      ];
   
      localStorage.setItem("myGifos", JSON.stringify({ data: objGifos }));
   }

   updateInformationGifo(getDataJSON.data.id, getDataJSON.data.images.fixed_height.url, title);
   
}

let updateInformationGifo = (id, src, title) =>{

   const createContainerButtons = document.querySelector('.create__buttons');
   const img = document.querySelector('.create__img');
   const objButtons = {
      className:'create',
      createContainerButtons,
      typeTheme: 'light',
      id,
   };

   img.src = src
   img.dataset.nameDownload = title

   createContainerButtons.dataset.gifId = id;

   createButtonCards(objButtons);
   
   recorder.destroy();
}

export {
   requestCameraPermissions,
   setMessageStep,
}
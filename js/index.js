import { config } from "./data.js";
import { requestCameraPermissions, setMessageStep } from "./creationGifo.js";
import { createCardBySection } from "./cards.js";
import { isMenuActive, validationScreen,validationTheme } from "./validations.js";

validationTheme();
const { API_KEY, URL_GIPHY } = config;
const mainSearch = document.querySelector(".home__inputSearch");
const btnMoreResults = document.querySelector(".results__more");
const btnMoreFavorites = document.querySelector(".favorites__more");
const btnMoreGifos = document.querySelector(".gifos__more");
const homeSearchBar = document.querySelector(".home__search");
const headerSearchBar = document.querySelector(".header__search");
const resultsContainer = document.querySelector(".results__container");
const favoritesContainer = document.querySelector(".favorites__container");
const gifosContainer = document.querySelector(".gifos__container");
const sectionHome = document.querySelector(".home");
const sectionFavorites = document.querySelector(".favorites");
const sectionMyGifos = document.querySelector(".gifos");
const sectionCreate = document.querySelector(".create");
const sectionResults = document.querySelector(".results");
const sectionTrending = document.querySelector(".trending");

const trendingCards = document.querySelector(".trending__cards");
const navigationBar = document.querySelector(".header__nav");
const logo = document.querySelector(".header__logo");
const btnMenu = document.querySelector(".header__label");
const body = document.querySelector("body");
let typeTheme = body.dataset.typeTheme;

let offset = 12;
let offsetFavorites = 0;
let offsetgifos = 0;
let term = "";

let arrIconsMenu = {
   menu: {
      srcLight: "./img/icon-menu-light.svg",
      srcDark: "./img/icon-menu-dark.svg",
   },
   close: {
      srcLight: "./img/icon-close-light.svg",
      srcDark: "./img/icon-close-dark.svg",
   },
};

let getSuggestions = async (e) => {
   const term = e.target.value;
   validateSearchBar();
   if (term !== "") {
      const endpoint = `${URL_GIPHY}/gifs/search/tags?q=${term}&api_key=${API_KEY}`;
      const getData = await fetch(endpoint);
      const getDataJSON = await getData.json();
      setSuggestions(getDataJSON);
   }
};

let setSuggestions = (getDataJSON) => {
   const status = getDataJSON.meta.status;
   const srcImg = "./img/icon-search-inactive.svg";
   const context = document.querySelector(".home__suggestions");
   context.innerHTML = "";
   context.classList.add("home__suggestions--active");

   const containerSearch = document.querySelector(".home__search");
   containerSearch.classList.add("home__search--active");

   const iconSearch = document.querySelector(".home__iconSearch");
   iconSearch.classList.remove("home__iconSearch--inactive");

   if (status === 200) {
      const createImg = document.createElement("IMG");

      if (!getDataJSON.data.length > 0) {
         let createLi = document.createElement("LI");
         createImg.classList.add("home__icon");
         createImg.setAttribute("src", srcImg);
         createImg.setAttribute("alt", "icono de una lupa");

         createLi.classList.add("home__li--inactive");
         createLi.classList.add("home__li");
         createLi.setAttribute(
            "data-suggestions-value",
            "No se encontraron sugerencias."
         );
         createLi.appendChild(createImg);
         createLi.innerHTML += "No se encontraron sugerencias.";
         context.appendChild(createLi);
      } else {
         getDataJSON.data.forEach((suggestion) => {
            let createLi = document.createElement("LI");

            createImg.classList.add("home__icon");
            createImg.setAttribute("src", srcImg);
            createImg.setAttribute("alt", "icono de una lupa");

            createLi.classList.add("home__li");
            createLi.setAttribute("data-term-value", suggestion.name);
            createLi.appendChild(createImg);
            createLi.innerHTML += suggestion.name;

            context.appendChild(createLi);
         });
         addEventClick(context);
      }
   }
};

let addEventClick = (context) => {
   let allTerms = context.querySelectorAll("LI");
   allTerms.forEach((terms) => {
      terms.addEventListener("click", assignTerm);
   });
};

let assignTerm = (e) => {
   term = e.target.dataset.termValue;
   mainSearch.value = term;
   resultsContainer.innerHTML = "";
   getGifResults(term, 0);
};

let getGifResults = async (term, offset) => {
   const endpoint = `${URL_GIPHY}/gifs/search?api_key=${API_KEY}&q=${term}&rating=g&offset=${offset}&limit=12`;
   const getData = await fetch(endpoint);
   const getDataJSON = await getData.json();
   const titleResults = document.querySelector(".results__title");
   titleResults.innerHTML = term;
   setGifResults(getDataJSON);
};

let setGifResults = (getDataJSON) => {
   const containerResults = document.querySelector(".results");
   const status = getDataJSON.meta.status;
   const maxLimitOffset =
      getDataJSON.pagination.count + getDataJSON.pagination.offset ==
      getDataJSON.pagination.total_count;

   containerResults.classList.add("results--active");

   if (status === 200) {
      if (!getDataJSON.data.length > 0) {
         const srcImg = "./img/icon-search-no-content.svg";
         const createNoContent = document.createElement("DIV");
         const createImg = document.createElement("IMG");
         const createMessage = document.createElement("P");

         createNoContent.classList.add("results__noContent");

         createImg.setAttribute("src", srcImg);
         createImg.setAttribute("alt", 'Ícono con el texto "ouch".');

         createMessage.classList.add("results__message");
         createMessage.innerHTML = "Intenta con otra búsqueda.";

         createNoContent.appendChild(createImg);
         createNoContent.appendChild(createMessage);

         resultsContainer.innerHTML = "";
         resultsContainer.appendChild(createNoContent);
         btnMoreResults.classList.remove("results__more--active");
      } else {
         const objGetDataJSON = {
            className: "results",
            context: resultsContainer,
            getDataJSON,
            typeTheme,
         };

         createCardBySection(objGetDataJSON);

         if (
            getDataJSON.pagination.total_count - getDataJSON.pagination.offset >
            1
         ) {
            btnMoreResults.classList.add("results__more--active");
         }
         if (maxLimitOffset) {
            btnMoreResults.classList.remove("results__more--active");
         }
      }
   }
};

let getTrendingWorks = async () => {
   const endpoint = `${URL_GIPHY}/trending/searches?api_key=${API_KEY}`;
   const getData = await fetch(endpoint);
   const getDataJSON = await getData.json();
   setTrendingWorks(getDataJSON);
};

let setTrendingWorks = (getDataJSON) => {
   const status = getDataJSON.meta.status;
   const context = document.querySelector(".home__topics");
   const limit = 5;

   if (status === 200) {
      for (let i = 0; i < limit; i++) {
         const work = getDataJSON.data[i];
         let createLI = document.createElement("LI");
         createLI.classList.add("home__topic");
         createLI.innerHTML = `${work}${i === 4 ? "" : ","}`;
         createLI.setAttribute("data-term-value", work);
         context.appendChild(createLI);
      }
      addEventClick(context);
   }
};

let getTrendingGifs = async () => {
   const endpoint = `${URL_GIPHY}/gifs/trending?api_key=${API_KEY}&limit=12&rating=g`;
   const getData = await fetch(endpoint);
   const getDataJSON = await getData.json();
   setTrendingGifs(getDataJSON);
};

let setTrendingGifs = (getDataJSON) => {
   const status = getDataJSON.meta.status;
   const arrows = document.querySelectorAll(".trending__arrow");
   if (status === 200) {
      if (!getDataJSON.data.length > 0) {
         const srcImg = "./img/icon-search-no-content.svg";
         const createNoContent = document.createElement("DIV");
         const createImg = document.createElement("IMG");
         const createMessage = document.createElement("P");

         createNoContent.classList.add("results__noContent");

         createImg.setAttribute("src", srcImg);
         createImg.setAttribute("alt", 'Ícono con el texto "ouch".');

         createMessage.classList.add("results__message");
         createMessage.innerHTML = "Intenta con otra búsqueda.";

         createNoContent.appendChild(createImg);
         createNoContent.appendChild(createMessage);

         resultsContainer.innerHTML = "";
         resultsContainer.appendChild(createNoContent);
         btnMoreResults.classList.remove("results__more--active");
      } else {
         const objGetDataJSON = {
            className: "trending",
            context: trendingCards,
            getDataJSON,
            typeTheme,
         };
         createCardBySection(objGetDataJSON);

         arrows.forEach((arrow) => {
            arrow.addEventListener("click", moveSlider);
         });
      }
   }
};

let seeMore = (typeSection) => {
   switch (typeSection) {
      case "Results":
         offset += 12;
         getGifResults(term, offset);
         break;
      case "Favorites":
         offsetGifFavorites();
         break;
      case "MyGifos":
         offsetGifos();
         break;
   }
};

let search = (e) => {
   const eventType = e.type;
   const context = e.target.parentNode;

   if (eventType === "keyup") {
      const keyCode = e.key;
      if (keyCode === "Enter") {
         if (
            e.target.classList.contains("header__inputSearch") ||
            e.target.classList.contains("home__inputSearch")
         ) {
            term = e.target.value;
            if (term !== "") {
               resultsContainer.innerHTML = "";
               getGifResults(term, 0);
            }
         }
      }
   } else if (eventType === "click") {
      if (
         e.target.classList.contains("header__iconSearch") ||
         e.target.dataset.status == "search"
      ) {
         term = context.querySelector("INPUT").value;
         if (term !== "") {
            resultsContainer.innerHTML = "";
            getGifResults(term, 0);
         }
      } else if (e.target.dataset.status == "close") {
         context.querySelector("INPUT").value = "";
         validateSearchBar();
      }
   }
};

let validateSearchBar = () => {
   const inputSearch = homeSearchBar.querySelector(".home__inputSearch");
   const containerSuggestions = homeSearchBar.querySelector(
      ".home__suggestions"
   );
   const iconSearchInactive = homeSearchBar.querySelector(".home__iconSearch");
   const iconSearchChange = homeSearchBar.querySelector("#iconSearchChange");

   if (inputSearch.value === "") {
      containerSuggestions.classList.remove("home__suggestions--active");
      iconSearchInactive.classList.add("home__iconSearch--inactive");
      iconSearchChange.setAttribute("src", (typeTheme === 'light') ? "./img/icon-search-light.svg": "./img/icon-search-dark.svg");
      iconSearchChange.dataset.status = "search";
      iconSearchChange.classList.remove("home__iconSearch--close");
   } else {
      containerSuggestions.classList.add("home__suggestions--active");
      iconSearchInactive.classList.remove("home__iconSearch--inactive");
      iconSearchChange.setAttribute("src", (typeTheme === 'light') ? "./img/icon-close-light.svg" : "./img/icon-close-dark.svg" );
      iconSearchChange.dataset.status = "close";
      iconSearchChange.classList.add("home__iconSearch--close");
   }
};

let moveSlider = (event) => {
   const arrowDirection = event.target.dataset.arrowDirection;
   if (event.target.localName == "span") {
      if (arrowDirection === "right") {
         trendingCards.scrollLeft += 369;
      } else {
         trendingCards.scrollLeft -= 369;
      }
   }
};

let setGifFavorites = (getDataJSON) => {
   if (!getDataJSON.data.length > 0) {
      const srcImg = "./img/icon-favorite-no-content.svg";
      const createNoContent = document.createElement("DIV");
      const createImg = document.createElement("IMG");
      const createMessage = document.createElement("P");

      createNoContent.classList.add("favorites__noContent");

      createImg.setAttribute("src", srcImg);
      createImg.setAttribute("alt", "Ícono con la imagen de un corazón.");

      createMessage.classList.add("favorites__message");
      createMessage.innerHTML =
         '"¡Guarda tu primer GIFO en favoritos para que se muestre aquí!"';

      createNoContent.appendChild(createImg);
      createNoContent.appendChild(createMessage);

      favoritesContainer.innerHTML = "";
      favoritesContainer.appendChild(createNoContent);
      btnMoreFavorites.classList.remove("favorites__more--active");
   } else {
      const objGetDataJSON = {
         className: "favorites",
         context: favoritesContainer,
         getDataJSON,
         typeTheme,
      };

      createCardBySection(objGetDataJSON);
      if (
         getDataJSON.data.length - offsetFavorites > 1 ||
         getDataJSON.lengthTotal - offsetFavorites > 1
      ) {
         btnMoreFavorites.classList.add("favorites__more--active");
         offsetFavorites += 12;
         if (
            getDataJSON.data.length < 12 ||
            getDataJSON.lengthTotal - offsetFavorites == 0
         ) {
            btnMoreFavorites.classList.remove("favorites__more--active");
         }
      } else {
         btnMoreFavorites.classList.remove("favorites__more--active");
         offsetFavorites = 0;
      }
   }
};

let getGifFavorites = () => {
   try {
      const isLocalStorage =
         JSON.parse(localStorage.getItem("favorites")) === null ? false : true;
      let getDataJSON;
      if (isLocalStorage) {
         getDataJSON = {
            data: JSON.parse(localStorage.getItem("favorites")).data,
         };
      } else {
         getDataJSON = { data: [] };
         localStorage.setItem("favorites", JSON.stringify(getDataJSON));
      }
      setGifFavorites(getDataJSON);
   } catch (error) {
      console.error(error);
   }
};

let getMyGifos = () => {
   try {
      const isLocalStorage =
         JSON.parse(localStorage.getItem("myGifos")) === null ? false : true;
      let getDataJSON;
      if (isLocalStorage) {
         getDataJSON = {
            data: JSON.parse(localStorage.getItem("myGifos")).data,
         };
      } else {
         getDataJSON = { data: [] };
         localStorage.setItem("myGifos", JSON.stringify(getDataJSON));
      }
      setGifMyGifos(getDataJSON);
   } catch (error) {
      console.error(error);
   }
};

let setGifMyGifos = (getDataJSON) => {
   if (getDataJSON.data.length === 0) {
      const srcImg = "./img/icon-myGifos-no-content.svg";
      const createNoContent = document.createElement("DIV");
      const createImg = document.createElement("IMG");
      const createMessage = document.createElement("P");

      createNoContent.classList.add("gifos__noContent");

      createImg.setAttribute("src", srcImg);
      createImg.setAttribute("alt", "Ícono con la imagen de un emoticon.");

      createMessage.classList.add("gifos__message");
      createMessage.innerHTML = "¡Anímate a crear tu primer GIFO!";

      createNoContent.appendChild(createImg);
      createNoContent.appendChild(createMessage);

      gifosContainer.innerHTML = "";
      gifosContainer.appendChild(createNoContent);
      btnMoreGifos.classList.remove("gifos__more--active");
   } else {
      const objGetDataJSON = {
         className: "gifos",
         context: gifosContainer,
         getDataJSON,
         typeTheme,
      };

      createCardBySection(objGetDataJSON);

      if (
         getDataJSON.data.length - offsetgifos > 1 ||
         getDataJSON.lengthTotal - offsetgifos > 1
      ) {
         btnMoreGifos.classList.add("gifos__more--active");
         offsetgifos += 12;
         if (
            getDataJSON.data.length < 12 ||
            getDataJSON.lengthTotal - offsetgifos == 0
         ) {
            btnMoreGifos.classList.remove("gifos__more--active");
         }
      } else {
         btnMoreGifos.classList.remove("gifos__more--active");
         offsetgifos = 0;
      }
   }
};

let offsetGifFavorites = () => {
   const dataLocalStorage = JSON.parse(localStorage.getItem("favorites")).data;
   const length = dataLocalStorage.length;
   const maxIteration = length - offsetFavorites;
   let getDataJSON = {
      data: [],
      lengthTotal: length,
   };

   for (let i = 0; i < (maxIteration > 12 ? 12 : maxIteration); i++) {
      getDataJSON.data.push(dataLocalStorage[offsetFavorites + i]);
   }
   setGifFavorites(getDataJSON);
};

let offsetGifos = () => {
   const dataLocalStorage = JSON.parse(localStorage.getItem("myGifos")).data;
   const length = dataLocalStorage.length;
   const maxIteration = length - offsetgifos;
   let getDataJSON = {
      data: [],
      lengthTotal: length,
   };

   for (let i = 0; i < (maxIteration > 12 ? 12 : maxIteration); i++) {
      getDataJSON.data.push(dataLocalStorage[offsetgifos + i]);
   }
   setGifMyGifos(getDataJSON);
};

let removeClassActiveMenu = (element) =>{
   const menuGifos = document.querySelector('#menuGifos');
   const menuFavoritos = document.querySelector('#menuFavoritos');
   const menuCrear = document.querySelector('#menuCrear');
   switch (element) {
      case 'favoritos':
         menuGifos.classList.remove('header__li--active');
         menuCrear.classList.remove('header__circle--active');
         menuFavoritos.classList.add('header__li--active');
         break;
      case 'gifos':
         menuFavoritos.classList.remove('header__li--active');
         menuCrear.classList.remove('header__circle--active');
         menuGifos.classList.add('header__li--active');
         break;
      case 'crear':
         menuFavoritos.classList.remove('header__li--active');
         menuGifos.classList.remove('header__li--active');
         menuCrear.classList.add('header__circle--active');
         break;
   }
}

let validateSectionActive = (event) => {
   const typeElement = event.target.dataset.typeElement;
   if (typeElement == "Enlace") {
      const typeSection = event.target.dataset.typeSection;
      switch (typeSection) {
         case "Inicio":
            location.reload();
            break;
         case "Modo Nocturno":
            const isDarkActive = body.dataset.typeTheme === 'dark';
            const newStatusTheme = (isDarkActive === true) ? 'light' : 'dark'
            const iconSearchChange = homeSearchBar.querySelector("#iconSearchChange");
            const style = document.querySelector('link');
            const title = document.querySelector('#modeTheme');
            const iconMenu = document.querySelector(".header__iconMenu");

            if(newStatusTheme === 'dark'){
               iconSearchChange.src = './img/icon-search-dark.svg';
               logo.src = "./img/ilustration-logo-dark.svg";
               style.href = "./css/style-dark.css";
               title.innerHTML = 'MODO DIURNO';
               typeTheme = 'dark';
               if(btnMenu.dataset.statusMenu === "activo"){
                  iconMenu.src = arrIconsMenu.close.srcDark;
               }else{
                  iconMenu.src = arrIconsMenu.menu.srcDark;
               }
               localStorage.setItem('theme', 'light');
            }else{
               iconSearchChange.src = './img/icon-search-light.svg';
               logo.src = "./img/ilustration-logo-light.svg";
               style.href = "./css/style-light.css";
               title.innerHTML = 'MODO NOCTURNO';
               typeTheme = 'light';
               if(btnMenu.dataset.statusMenu === "activo"){
                  iconMenu.src = arrIconsMenu.close.srcLight;
               }else{
                  iconMenu.src = arrIconsMenu.menu.srcLight;
               }
               localStorage.setItem('theme', 'dark');
            }
            
            body.dataset.typeTheme =  newStatusTheme;
            break;
         case "Favoritos":
            sectionHome.classList.remove("home--active");
            sectionMyGifos.classList.remove("gifos--active");
            sectionCreate.classList.remove("create--active");
            sectionResults.classList.remove("results--active");
            sectionFavorites.classList.add("favorites--active");
            sectionTrending.classList.remove("trending--inactive");
            favoritesContainer.innerHTML = "";
            getGifFavorites();
            removeClassActiveMenu('favoritos');
            break;
         case "Mis Gifos":
            sectionHome.classList.remove("home--active");
            sectionFavorites.classList.remove("favorites--active");
            sectionCreate.classList.remove("create--active");
            sectionResults.classList.remove("results--active");
            sectionMyGifos.classList.add("gifos--active");
            sectionTrending.classList.remove("trending--inactive");
            gifosContainer.innerHTML = "";
            getMyGifos();
            removeClassActiveMenu('gifos');
            break;
         case "Crear":
            sectionHome.classList.remove("home--active");
            sectionFavorites.classList.remove("favorites--active");
            sectionMyGifos.classList.remove("gifos--active");
            sectionResults.classList.remove("results--active");
            sectionTrending.classList.add("trending--inactive");
            sectionCreate.classList.add("create--active");
            setMessageStep("0");
            removeClassActiveMenu('crear');
            const btnComenzar = document.querySelector("#btnCreate");
            btnComenzar.addEventListener("click", requestCameraPermissions);
            break;
      }
   }
};

let actionsMenu = (event) => {
   const iconMenu = document.querySelector(".header__iconMenu");
   const getValues = validationScreen();
   const widthValue = getValues.width;
   if (isMenuActive(event) && widthValue <= 1200) {
      sectionFavorites.classList.add("zIndexHidden");
      sectionMyGifos.classList.add("zIndexHidden");
      sectionCreate.classList.add("zIndexHidden");
      iconMenu.src =
         typeTheme === "light"
            ? arrIconsMenu.close.srcLight
            : arrIconsMenu.close.srcDark;
      body.classList.add("body-inactive");
   } else {
      sectionFavorites.classList.remove("zIndexHidden");
      sectionMyGifos.classList.remove("zIndexHidden");
      sectionCreate.classList.remove("zIndexHidden");
      iconMenu.src =
         typeTheme === "light"
            ? arrIconsMenu.menu.srcLight
            : arrIconsMenu.menu.srcDark;
      body.classList.remove("body-inactive");
   }
};
let event = {
   target:{
      dataset:{
         typeElement: 'Enlace',
         typeSection: 'Modo Nocturno'
      }
   }
}
validateSectionActive(
   event
);
getTrendingWorks();
getTrendingGifs();

navigationBar.addEventListener("click", validateSectionActive);
logo.addEventListener("click", validateSectionActive);

btnMenu.addEventListener("click", actionsMenu);
mainSearch.addEventListener("keyup", getSuggestions);

btnMoreResults.addEventListener("click", () => {
   seeMore("Results");
});
btnMoreFavorites.addEventListener("click", () => {
   seeMore("Favorites");
});
btnMoreGifos.addEventListener("click", () => {
   seeMore("MyGifos");
});

homeSearchBar.addEventListener("click", search);
homeSearchBar.addEventListener("keyup", search);

headerSearchBar.addEventListener("keyup", search);
headerSearchBar.addEventListener("click", search);

headerSearchBar.addEventListener("click", search);

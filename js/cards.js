import { validateLocalStorage, validationScreen } from "./validations.js";

const arrSrcIcons = [
   {
      type: "favorite",
      light: {
         src: "./img/icon-favorite-light.svg",
         srcA: "./img/icon-favorite-light-active.svg",
      },
      dark: {
         src: "./img/icon-favorite-dark.svg",
         srcA: "./img/icon-favorite-dark-active.svg",
      },
   },
   {
      type: "download",
      light: {
         src: "./img/icon-download-light.svg",
      },
      dark: {
         src: "./img/icon-download-dark.svg",
      },
   },
   {
      type: "maximize",
      light: {
         src: "./img/icon-maximize.svg",
         srcClose: "./img/icon-close-light.svg",
      },
      dark: {
         srcClose: "./img/icon-close-dark.svg",
      },
   },
   {
      type: "link",
      light: {
         src: "./img/icon-link.svg",
      },
   },{
      type: "delete",
      light:{
         src: "./img/icon-delete.svg"
      }
   }
];
const modalMaximize = document.querySelector(".modal");
const body = document.querySelector("body");
const typeTheme = () =>{
   let getStatusTheme = document.querySelector('body').dataset.typeTheme;
   return getStatusTheme;
};

let createCardBySection = (objGetDataJSON) => {
   if (objGetDataJSON.className === "modal") {
      createCard(objGetDataJSON);
   } else {
      const {
         getDataJSON,
         getDataJSON: {
            data: { length },
         },
         context,
         className,
         typeTheme,
      } = objGetDataJSON;
      const limit = length > 12 ? 12 : length;
      for (let i = 0; i < limit; i++) {
         const {
            username,
            title,
            id,
            images: {
               fixed_height: { url: srcImg },
            },
         } = getDataJSON.data[i];
         const objCard = {
            username,
            title,
            id,
            srcImg,
            context,
            className,
            typeTheme,
         };
         createCard(objCard);
      }
   }
};

let createCard = (objCard) => {
   const {
      username,
      title,
      id,
      srcImg,
      context,
      className,
      typeTheme,
   } = objCard;

   if (className == "modal") {
      const modalContainer = document.createElement("DIV");
      const modalClose = document.createElement("SPAN");
      const modalIconClose = document.createElement("IMG");
      const modalImg = document.createElement("IMG");
      const modalContainerOptions = document.createElement("DIV");
      const modalInfo = document.createElement("DIV");
      const modalAuthor = document.createElement("P");
      const modalTitleGifo = document.createElement("P");
      const modalButtons = document.createElement("DIV");

      modalIconClose.classList.add("modal__icon-close");
      modalIconClose.setAttribute(
         "src",
         typeTheme() === "light"
            ? arrSrcIcons[2].light.srcClose
            : arrSrcIcons[2].dark.srcClose
      );
      modalIconClose.setAttribute("alt", "Icono de cerrar modal");

      modalImg.classList.add("modal__img");
      modalImg.setAttribute("src", srcImg);
      modalImg.setAttribute(
         "alt",
         `Imagen referente a: ${title === "" ? "Gif sin descripción" : title}`
      );
      modalImg.setAttribute(
         "data-name-download",
         title === "" ? "MyGif" : title
      );

      modalAuthor.classList.add("modal__author");
      modalAuthor.textContent = username;

      modalTitleGifo.classList.add("modal__titleGifo");
      modalTitleGifo.textContent = title;

      modalInfo.appendChild(modalAuthor);
      modalInfo.appendChild(modalTitleGifo);

      modalInfo.classList.add("modal__info");
      modalInfo.appendChild(modalAuthor);
      modalInfo.appendChild(modalTitleGifo);

      modalButtons.classList.add("modal__buttons");
      modalButtons.setAttribute("data-gif-id", id);
      modalButtons.setAttribute("data-class-name", className);

      const objButtons = {
         className,
         createContainerButtons: modalButtons,
         id,
         typeTheme,
      };

      createButtonCards(objButtons);

      modalContainerOptions.classList.add("modal__container-options");
      modalContainerOptions.appendChild(modalInfo);
      modalContainerOptions.appendChild(modalButtons);

      modalClose.classList.add("modal__close");
      modalClose.appendChild(modalIconClose);
      modalClose.addEventListener("click", closeModal);

      modalContainer.classList.add("modal__container");
      modalContainer.appendChild(modalClose);
      modalContainer.appendChild(modalImg);
      modalContainer.appendChild(modalContainerOptions);

      context.appendChild(modalContainer);
   } else {
      const createCard = document.createElement("DIV");
      const createImg = document.createElement("IMG");
      const createContainerButtons = document.createElement("DIV");
      const createContainerInfo = document.createElement("DIV");
      const createAuthor = document.createElement("P");
      const createTitleGifo = document.createElement("P");
      const objButtons = {
         className,
         createContainerButtons,
         typeTheme,
         id,
      };
      createButtonCards(objButtons);

      createContainerButtons.classList.add(`${className}__buttons`);
      createContainerButtons.setAttribute("data-gif-id", id);
      createContainerButtons.setAttribute("data-class-name", className);

      createImg.classList.add(`${className}__img`);
      createImg.setAttribute("src", srcImg);
      createImg.setAttribute(
         "alt",
         `Imagen referente a: ${title === "" ? "Gif sin descripción" : title}`
      );
      createImg.setAttribute(
         "data-name-download",
         title === "" ? "MyGif" : title
      );

      createAuthor.classList.add(`${className}__author`);
      createAuthor.innerHTML = username === "" ? "Anónimo" : username;

      createTitleGifo.classList.add(`${className}__titleGifo`);
      createTitleGifo.innerHTML = title === "" ? "Anónimo" : title;

      createContainerInfo.classList.add(`${className}__info`);
      createContainerInfo.appendChild(createAuthor);
      createContainerInfo.appendChild(createTitleGifo);

      createCard.classList.add(`${className}__card`);
      createCard.appendChild(createImg);
      createCard.appendChild(createContainerButtons);
      createCard.appendChild(createContainerInfo);

      context.appendChild(createCard);

      createCard.addEventListener("click", () => {
         const getValues = validationScreen();
         const widthValue = getValues.width;
         if (widthValue <= 1200) {
            maximizeGif(createCard, className);
         }
      });
   }
};

let createButtonCards = (objButtons) => {
   const { className, createContainerButtons, typeTheme, id } = objButtons;

   switch (className) {
      case "modal":
         for (let i = 0; i < 2; i++) {
            const createSpan = document.createElement("A");
            const createIcon = document.createElement("IMG");

            createIcon.classList.add(`${className}__icon`);
            createIcon.setAttribute(
               "src",
               typeTheme() === "light"
                  ? arrSrcIcons[i].light.src
                  : arrSrcIcons[i].dark.src
            );
            createIcon.setAttribute("data-type-button", arrSrcIcons[i].type);

            if (i == 0 && validateFavorites(id)) {
               createIcon.setAttribute(
                  "src",
                  typeTheme() === "light"
                     ? arrSrcIcons[i].light.srcA
                     : arrSrcIcons[i].dark.srcA
               );
               createIcon.classList.add("active");
            }

            createSpan.classList.add(`${className}__button`);
            createSpan.appendChild(createIcon);

            createSpan.addEventListener("click", addEventsCardButtons);

            createContainerButtons.appendChild(createSpan);
         }
         break;
      case "create":
         for (let i = 1; i <= 3; i++) {
            if (!(i == 2)) {
               const createSpan = document.createElement("A");
               const createIcon = document.createElement("IMG");

               createIcon.classList.add(`${className}__icon`);
               createIcon.setAttribute("src", arrSrcIcons[i].light.src);
               createIcon.setAttribute("data-type-button", arrSrcIcons[i].type);

               createSpan.classList.add(`${className}__button`);
               createSpan.appendChild(createIcon);

               createSpan.addEventListener("click", addEventsCardButtons);

               createContainerButtons.appendChild(createSpan);
            }
         }
         break;
      case "gifos":
         for (let i = 0; i < 3; i++) {
            const createSpan = document.createElement("A");
            const createIcon = document.createElement("IMG");

            createIcon.classList.add(`${className}__icon`);

            if (arrSrcIcons[i].type == "maximize") {
               createIcon.setAttribute("src", arrSrcIcons[i].light.src);
            } else {
               createIcon.setAttribute("src", arrSrcIcons[i].light.src);
            }
            
            if (i == 0 ) {
               createIcon.setAttribute("data-type-button", arrSrcIcons[4].type);
               createIcon.setAttribute("src", arrSrcIcons[4].light.src);
               createIcon.classList.add("active");
            }else{
               createIcon.setAttribute("data-type-button", arrSrcIcons[i].type);
            }

            createSpan.classList.add(`${className}__button`);
            createSpan.appendChild(createIcon);

            createSpan.addEventListener("click", addEventsCardButtons);

            createContainerButtons.appendChild(createSpan);
         }
         break
         default:
         for (let i = 0; i < 3; i++) {
            const createSpan = document.createElement("A");
            const createIcon = document.createElement("IMG");

            createIcon.classList.add(`${className}__icon`);

            if (arrSrcIcons[i].type == "maximize") {
               createIcon.setAttribute("src", arrSrcIcons[i].light.src);
            } else {
               createIcon.setAttribute("src", arrSrcIcons[i].light.src);
            }

            createIcon.setAttribute("data-type-button", arrSrcIcons[i].type);

            if (i == 0 && validateFavorites(id)) {
               createIcon.setAttribute("src", arrSrcIcons[i].light.srcA);
               createIcon.classList.add("active");
            }

            createSpan.classList.add(`${className}__button`);
            createSpan.appendChild(createIcon);

            createSpan.addEventListener("click", addEventsCardButtons);

            createContainerButtons.appendChild(createSpan);
         }
         break;
   }
};

let addEventsCardButtons = (e) => {
   const element = e.target;
   let context = element.parentNode.parentNode.parentNode;
   let typeButton = element.dataset.typeButton;
   let id = element.parentNode.parentNode.dataset.gifId;
   let className = element.parentNode.parentNode.dataset.className;

   if (element.classList.contains("modal__icon")) {
      context = element.parentNode.parentNode.parentNode.parentNode;
      typeButton = element.dataset.typeButton;
      id = element.parentNode.parentNode.dataset.gifId;
      className = element.parentNode.parentNode.dataset.className;
   }

   switch (typeButton) {
      case "favorite":
         const allButtonsForId = document.querySelectorAll(
            `[data-gif-id ="${id}"]`
         );

         element.classList.toggle("active");

         if (element.classList.contains("active")) {
            if (className === "modal") {
               element.setAttribute(
                  "src",
                  typeTheme() === "light"
                     ? arrSrcIcons[0].light.srcA
                     : arrSrcIcons[0].dark.srcA
               );
            } else {
               element.setAttribute("src", arrSrcIcons[0].light.srcA);
            }
            allButtonsForId.forEach((container) => {
               let btnFavorite = container.querySelector(
                  '[data-type-button = "favorite"]'
               );

               if (btnFavorite.classList.contains("modal__icon")) {
                  btnFavorite.setAttribute(
                     "src",
                     typeTheme() === "light"
                        ? arrSrcIcons[0].light.srcA
                        : arrSrcIcons[0].dark.srcA
                  );
               } else {
                  btnFavorite.setAttribute("src", arrSrcIcons[0].light.srcA);
               }

               btnFavorite.classList.add("active");
            });
            addToFavorites(context, className);
         } else {
            if (className === "modal") {
               element.setAttribute(
                  "src",
                  typeTheme() === "light"
                     ? arrSrcIcons[0].light.src
                     : arrSrcIcons[0].dark.src
               );
            } else {
               element.setAttribute("src", arrSrcIcons[0].light.src);
            }
            allButtonsForId.forEach((container) => {
               let btnFavorite = container.querySelector(
                  '[data-type-button = "favorite"]'
               );
               if (btnFavorite.classList.contains("modal__icon")) {
                  btnFavorite.setAttribute(
                     "src",
                     typeTheme() === "light"
                        ? arrSrcIcons[0].light.src
                        : arrSrcIcons[0].dark.src
                  );
               } else {
                  btnFavorite.setAttribute("src", arrSrcIcons[0].light.src);
               }
               btnFavorite.classList.remove("active");
            });
            removeToFavorites(id);
         }
         break;
      case "download":
         downloadGif(context, element);
         break;
      case "maximize":
         maximizeGif(context, className);
         break;
      case "link": 
         let img = context.querySelector("[data-name-download]");
         let src = img.src;
         window.open(src);
         break
      case "delete": {
         const getDataGifos = JSON.parse(localStorage.getItem('myGifos'));
         let indexDelete = getDataGifos.data.findIndex((gifo) => gifo.id == id);
         getDataGifos.data.splice(indexDelete,1);
         localStorage.setItem("myGifos", JSON.stringify(getDataGifos))
         location.reload();
      }
   }
};

let addToFavorites = (context, className) => {
   if (validateLocalStorage("favorites")) {
      const getDataLocalStorage = JSON.parse(localStorage.getItem("favorites"));
      let objFavorite = {
         username: context.querySelector(`.${className}__author`).textContent,
         id: context.querySelector(`.${className}__buttons`).dataset.gifId,
         images: {
            fixed_height: {
               url: context.querySelector(`.${className}__img`).src,
            },
         },
         title: context.querySelector(`.${className}__titleGifo`).textContent,
      };

      getDataLocalStorage.data.push(objFavorite);
      localStorage.setItem("favorites", JSON.stringify(getDataLocalStorage));
   } else {
      let objFavorite = [
         {
            username: context.querySelector(`.${className}__author`)
               .textContent,
            id: context.querySelector(`.${className}__buttons`).dataset.gifId,
            images: {
               fixed_height: {
                  url: context.querySelector(`.${className}__img`).src,
               },
            },
            title: context.querySelector(`.${className}__titleGifo`)
               .textContent,
         },
      ];

      localStorage.setItem("favorites", JSON.stringify({ data: objFavorite }));
   }
};

let validateFavorites = (gifId) => {
   if (validateLocalStorage("favorites")) {
      const getDataLocalStorage = JSON.parse(localStorage.getItem("favorites"));

      let searchFavorite = getDataLocalStorage.data.find(
         (img) => img.id === gifId
      );

      return searchFavorite !== undefined ? true : false;
   }
};

let removeToFavorites = (gifId) => {
   const getDataLocalStorage = JSON.parse(localStorage.getItem("favorites"))
      .data;
   let searchFavorite = getDataLocalStorage.findIndex(
      (img) => img.id === gifId
   );
   getDataLocalStorage.splice(searchFavorite, 1);
   localStorage.setItem(
      "favorites",
      JSON.stringify({ data: getDataLocalStorage })
   );
};

let downloadGif = async (context, element) => {
   let img = context.querySelector("[data-name-download]");
   let src = img.src;
   let title = img.dataset.nameDownload;
   let button = element.parentNode;
   let getDataImg = await fetch(src);
   let imgBlob = await getDataImg.blob();
   let newURLBlob = URL.createObjectURL(imgBlob);

   button.setAttribute("download", title);
   button.href = newURLBlob;
   button.click();
};
let maximizeGif = (context, className) => {
   modalMaximize.classList.add("modal--active");
   body.classList.add("body-inactive");

   const objCard = {
      username: context.querySelector(`.${className}__author`).textContent,
      id: context.querySelector(`.${className}__buttons`).dataset.gifId,
      srcImg: context.querySelector(`.${className}__img`).src,
      title: context.querySelector(`.${className}__titleGifo`).textContent,
      className: "modal",
      context: modalMaximize,
      typeTheme,
   };
   createCardBySection(objCard);
};

let closeModal = () => {
   modalMaximize.classList.remove("modal--active");
   modalMaximize.innerHTML = "";
   body.classList.remove("body-inactive");
};
export { createCardBySection, createButtonCards };

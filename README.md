# Gifos

Este es el proyecto del segundo bloque de la carrera: Desarrollo Web FullStack.

### Pre-requisitos 📋

Unicamente es necesario bases en hojas de estilos css con Sass, metodología BEM, JavaScript y manejo de estructuras HTML

_Ejemplo:_
```
<p class="home__description--mobile">Un podcast que explora el mundo de la programación. Nuevos episodios, todos los jueves cada 15 días.</p>
```

## Construido con 🛠️

Estas fueron las herramientas usadas para el desarrollo de este proyecto:

* [Sass](https://sass-lang.com/) - El framework css usado
* [BEM](http://getbem.com/) - Metodología Css
* [Fontawesome](https://fontawesome.com/) - Fuente para icons
* [Google Fonts](https://fonts.google.com/) - Fuentes
* [Giphy](https://developers.giphy.com/) - API
* [RecordRTC](https://recordrtc.org/) - Para crear los gif's


## Versionado 📌

La indea es implementar [SemVer](http://semver.org/) para el versionado, por lo pronto se asigna la v0.0.1

## Licencia 📄

Este proyecto está bajo la Licencia  MIT .

## Demo 🖥

* [Gifos](https://davidmorenocode.co/projects/gifos)

## Capturas 📷

<img src="./img/docs/Cap1.png" width=800 />
<img src="./img/docs/Cap2.png" width=800 />
<img src="./img/docs/Cap3.png" width=800 />
<img src="./img/docs/Cap4.png" width=800 />
<img src="./img/docs/Cap5.png" width=800 />
<img src="./img/docs/Cap6.png" width=800 />
<img src="./img/docs/Cap7.png" width=800 />
<img src="./img/docs/Cap8.png" width=400 />
<img src="./img/docs/Cap9.png" width=400 />
<img src="./img/docs/Cap10.png" width=400 />
<img src="./img/docs/Cap11.png" width=400 />


---
⌨️ por [David Moreno](https://www.linkedin.com/in/davidmorenocode/) 🤓☕
